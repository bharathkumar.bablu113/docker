! /bin/bash

REGION='us-east-1'
INSTANCE_ID=$1
# Find all gp2 Volumes within given Instance ID
GP2_VOL_IDS=$(/usr/bin/aws ec2 describe-volumes --region us-east-1 --filters Name=attachment.instance-id,Values=$INSTANCE_ID Name=volume-type,Values=gp2 | jq -r '.Volumes[].VolumeId')
echo "Following are the list of GP2 volumes of given $INSTANCE_ID: $GP2_VOL_IDS"

# Iterate all gp2 volumes and change its type to gp3
for VOL_ID in ${GP2_VOL_IDS};do
    echo "Migrating the Volume ID: $VOL_ID to gp3"
    OUTPUT=$(/usr/bin/aws ec2 modify-volume --region "${REGION}" --volume-type=gp3 --volume-id "${VOL_ID}" | jq '.VolumeModification.ModificationState' | sed 's/"//g')
    if [ $? -eq 0 ] && [ "${OUTPUT}" == "modifying" ];then
        echo "INFO: volume ${VOL_ID} changed to state 'modifying'"
    else
        echo "ERROR: Manual Intervention is required. We are not able to change volume ${VOL_ID} type to gp3!"
    fi
done
