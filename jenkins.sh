set +x
echo "${Source_Key_Path}"

while IFS= read -r -u3 x; IFS= read -r -u4 y; do 
  echo "aws s3 cp s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y}  --profile dev --acl bucket-owner-full-control --recursive"
  aws s3 cp s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y}  --profile dev --acl bucket-owner-full-control --recursive
done 3<<<"${Source_Key_Path}" 4<<<"${Destination_Key_Path}"
