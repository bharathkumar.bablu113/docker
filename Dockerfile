FROM ubuntu
RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.11.2-amd64.deb
RUN dpkg elasticsearch-7.11.2-amd64.deb
RUN echo "echo "network.host: 0.0.0.0" >> elasticsearch.yml"
RUN echo "http.port: 9200" >> elasticsearch.yml
RUN echo "discovery.type: single-node" >> elasticsearch.yml
RUN systemctl enable elasticsearch
RUN systemctl start elasticsearch
EXPOSE 9200