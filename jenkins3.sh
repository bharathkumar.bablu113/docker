#!/bin/bash

Source_Bucket_Name=$1
Source_Key_Path=$2
Destination_Bucket_Name=$3
Destination_Key_Path=$4

log='test.log'
while IFS= read -r -u3 x; IFS= read -r -u4 y; do
  echo "***Copying file from s3://${Source_Bucket_Name}${x} to s3://${Destination_Bucket_Name}${y}"
  aws s3 cp --output text s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y} --profile dev --acl bucket-owner-full-control --recursive | tr "\r" "\n" > >(awk '{print strftime("%Y-%m-%d:%H:%M:%S ") $0}') | tee >> $log 2>&1
  RESULT=`cat test.log | grep 'Completed'`
  if [[ $RESULT != *"Completed"* ]]; then
    aws s3 cp --output text s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y}  --profile dev --acl bucket-owner-full-control | tr "\r" "\n" > >(awk '{print strftime("%Y-%m-%d:%H:%M:%S ") $0}') | tee >> $log 2>&1
  fi
done 3<<<"${Source_Key_Path}" 4<<<"${Destination_Key_Path}"
cat test.log
