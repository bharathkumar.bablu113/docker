set +x

while IFS= read -r -u3 x; IFS= read -r -u4 y; do 
  echo "***Copying file from s3://${Source_Bucket_Name}${x} to s3://${Destination_Bucket_Name}${y}"
  RESULT=`aws s3 cp s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y}  --profile dev --acl bucket-owner-full-control --recursive`
  if [[ $RESULT == *"Completed"* ]]; then
    echo "*****Copy Success: $RESULT"
  else
    RESULT=`aws s3 cp s3://${Source_Bucket_Name}${x} s3://${Destination_Bucket_Name}${y}  --profile dev --acl bucket-owner-full-control`    
    if [[ $RESULT == *"Completed"* ]]; then
       echo "*****Copy Success: $RESULT"
    else
       echo "*****Copy Failed: Source: s3://${Source_Bucket_Name}${x} Destination: s3://${Destination_Bucket_Name}${y}"
    fi
  fi
done 3<<<"${Source_Key_Path}" 4<<<"${Destination_Key_Path}"

